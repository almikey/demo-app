package com.adalabs.bouncer.db.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.adalabs.bouncer.model.User

@Database(
    entities = arrayOf(
        User::class
    ),
    version = 1,
    exportSchema = false
)
abstract class BounceRoomDatabase:RoomDatabase(){
    abstract fun userDao():UserDao
//    fun adsDao():AdsDao
}