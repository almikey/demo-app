package com.adalabs.bouncer.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    var name:String,
    var age:Int,
    @PrimaryKey(autoGenerate = true)
    var userId:Int = 0
)
