package com.adalabs.bouncer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class LoggedInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signed_in_page)
    }
}