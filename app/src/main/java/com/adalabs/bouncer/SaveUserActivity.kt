package com.adalabs.bouncer

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.adalabs.bouncer.db.dao.BounceRoomDatabase
import com.adalabs.bouncer.model.User

class SaveUserActivity : AppCompatActivity() {

    val theDb by lazy{
        Room.databaseBuilder(
            applicationContext,
            BounceRoomDatabase::class.java,
            "rewardads-db"
        ).build()
    }

    val userDao by lazy{theDb.userDao()}

    lateinit var nameInput: EditText
    lateinit var ageInput: EditText
    lateinit var submitBtn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.save_user_page)
        nameInput = findViewById(R.id.inputusername)
        ageInput = findViewById(R.id.inputage)
        submitBtn = findViewById(R.id.showUser)
        submitBtn.setOnClickListener {
            saveToDb(nameInput.text.toString(), ageInput.text.toString().toInt())
            var intent = Intent(this, SampleActivity::class.java)
            startActivity(intent)
        }
    }

    fun saveToDb(name: String, age: Int) {
        var user = User(name, age)
        userDao.saveUser(user)
    }

}