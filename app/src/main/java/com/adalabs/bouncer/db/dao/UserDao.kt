package com.adalabs.bouncer.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.adalabs.bouncer.model.User
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface UserDao {
    @Query("select * from user where userId=:name")
    fun fetchUsersByName(name: String): Flowable<List<User>>
//RX or LiveData
    @Insert()
    fun saveUser(user: User): Completable
}