package com.adalabs.bouncer

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.adalabs.bouncer.db.dao.BounceRoomDatabase
import io.reactivex.schedulers.Schedulers

class SampleActivity : AppCompatActivity() {

    val theDb by lazy {
        Room.databaseBuilder(
            applicationContext,
            BounceRoomDatabase::class.java,
            "rewardads-db"
        ).build()
    }

    val userDao by lazy{theDb.userDao()}

    lateinit var nameInput: EditText
    lateinit var searchButton: Button
    lateinit var showUsername: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.show_user_page)
        nameInput = findViewById(R.id.inputusername)
        searchButton = findViewById(R.id.submitsearch)
        showUsername = findViewById(R.id.showUser)
        searchButton.setOnClickListener {
            var result = searchUser(nameInput.text.toString())
            showUsername.text = result
        }

    }

    fun searchUser(name: String): String {
        userDao.fetchUsersByName(name)
            .subscribeOn(Schedulers.io())
            .observeOn(Android)
            .subscribe({userlist->
                showUsername.text = userlist[0].name
                Log.d("userlist",userlist.toString())
            },{error->

            })

//        var userRes = userDao.fetchUsersByName(name)
//        if (userRes.size == 0) {
//            return ""
//        } else {
//            return userRes[0].name
//        }
    }

}